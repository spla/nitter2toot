# nitter2toot
Toot to a Mastodon server the tweets of any Twitter user via Nitter's RSS feed. You don't need Twitter account at all.
Respect your Mastodon server users's privacy!

### Dependencies

-   **Python 3**
-   Postgresql server (only if you want to run Postgresql version)
-   Everything else at the top of `nitter2toot.py`!

### Usage:

Within Python Virtual Environment:

### Postgresql version

1. Run `python db-setup.py` to create needed database and table. It's needed to control what tweets are already tooted. db-setup.py
   will also ask you the Nitter's feed url.

2. Run `python setup.py` to get your bot's access token of a Mastodon existing account. It will be saved to 'secrets/secrets.txt' for further use.

3. Run `python nitter2toot.py` to start tooting tweets via Nitter's feed url of the user of your choice.

4. Use your favourite scheduling method to set `nitter2toot.py` to run regularly.

Note: install all needed packages with `pip install package` or use `pip install -r requirements.txt` to install them. 

### sqlite3 version

1. Run `python sqlite-setup.py` to create needed database and table. It's needed to control what tweets are already tooted. sqlite-setup.py
   will also ask you the Nitter's feed url.

2. Run `python setup.py` to get your bot's access token of a Mastodon existing account. It will be saved to `secrets/secrets.txt` for further use.

3. Run `python nitter2toot-sqlite.py` to start tooting tweets via Nitter's feed url of the user of your choice.

4. Use your favourite scheduling method to set `nitter2toot-sqlite.py` to run regularly.

Note: install all needed packages with `pip install package` or use `pip install -r sqlite-requirements.txt` to install them. 


