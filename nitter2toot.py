import os
import sys
import feedparser
from mastodon import Mastodon
import psycopg2
from psycopg2 import errors
from datetime import datetime, timedelta
import time

def date_diff_in_seconds(dt1, dt2):
    timedelta = dt2 - dt1
    days = timedelta.days
    return days, timedelta.days * 24 * 3600 + timedelta.seconds

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

########################################################
# main

if __name__ == '__main__':

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    feeds_db = get_parameter("feeds_db", config_filepath)
    feeds_db_user = get_parameter("feeds_db_user", config_filepath)
    feeds_url = get_parameter("feeds_url", config_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    now = datetime.now()
    publish = 0
    prev_feed_date = None
    days = 0
    seconds = None
    user = feeds_url[:-4]
    user_index = user.rfind('/')
    user = '@' + user[user_index + 1:]

    newsfeed = feedparser.parse(feeds_url)

    new_feeds_id = []
    new_feeds_title = []
    new_feeds_author = []
    new_feeds_link = []
    new_feeds_published = []

    for entry in reversed(newsfeed.entries[1:]):

        title = entry['title']
        id = int(entry['id'].split('status/')[1][:-2])
        author = entry['author']
        link = entry['link']
        published = entry['published']

        published_str = published.rstrip(' GMT')
        published_dt_ix = published_str.index(', ')
        published_dt = published_str[published_dt_ix + 2:]
        published_dt = datetime.strptime(published_dt, '%d %b %Y %H:%M:%S')

        if published_dt < now - timedelta(hours=24):

            continue

        try:

            conn = None
            conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute('select id from tweets where id=(%s)', (id,))

            row = cur.fetchone()
            if row == None:
                new_feeds_id.append(id)
                new_feeds_title.append(title)
                new_feeds_author.append(author)
                new_feeds_link.append(link)
                new_feeds_published.append(published)

            cur.close()

        except (Exception, psycopg2.errors.UndefinedColumn) as column_error:

            print(column_error)
            sys.exit("Run 'python update-db.py' to add missing columns\n")

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    i = 0
    while i < len(new_feeds_id):

        feed_published = datetime.strptime(new_feeds_published[i], '%a, %d %b %Y %H:%M:%S GMT')

        title = new_feeds_title[i]
        id = new_feeds_id[i]
        author = new_feeds_author[i]
        link = new_feeds_link[i]

        ###################################################################
        # get last published toot id and published datetime
        ###################################################################

        try:

            conn = None
            conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute('select published, tootid from tweets order by id desc limit 1') #get previous toot id and publish date
            row = cur.fetchone()
            if row != None:
                prev_feed_date = row[0]
                prev_id = row[1]
            else:
                prev_feed_date = None
                prev_id = None
                seconds = 10

            cur.close()

        except (Exception, psycopg2.errors.UndefinedColumn) as column_error:

            print(column_error)
            sys.exit("Run 'python update-db.py' to add missing columns\n")

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        ###########################################################

        if prev_feed_date != None:
            days, seconds = date_diff_in_seconds(prev_feed_date, feed_published)

        ###########################################################

        if "RT by " in title:
            toot_text = 'boosted ' + author + ':\n\n'
            title = title.replace("RT by "+user+":"+" ","")
            toot_text += title + '\n\n'
        else:
            toot_text = str(title)+'\n'
        toot_text += str(link)+'\n'
        toot_text += '\n'
        toot_text += str(feed_published)

        print("Tooting...")
        print(toot_text)

        if len(new_feeds_id) > 1:

            if seconds is not None and days == 0 and seconds < 5:
                toot_id = mastodon.status_post(toot_text, in_reply_to_id=prev_id,)
            else:
                toot_id = mastodon.status_post(toot_text, in_reply_to_id=None,)
            time.sleep(2)

        else:

            if seconds is not None and days == 0 and seconds < 5:
                toot_id = mastodon.status_post(toot_text, in_reply_to_id=prev_id,)
                time.sleep(2)
            else:
                toot_id = mastodon.status_post(toot_text, in_reply_to_id=None,)

        #########################################################

        insert_sql = 'INSERT INTO tweets(id, published, tootid) VALUES (%s,%s,%s) ON CONFLICT DO NOTHING'

        conn = None

        try:

            conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (id, feed_published, toot_id['id']))
            cur.execute('update tweets set published=(%s), tootid=(%s) where id=(%s)', (feed_published, toot_id['id'], id,))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        i += 1

    print("No new feeds")
