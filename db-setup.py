#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  if not os.path.exists('config'):
    os.makedirs('config')
  print("Setting up Nitter newsfeed parameters...")
  print("\n")
  feeds_db = input("feeds db name: ")
  feeds_db_user = input("feeds db user: ")
  feeds_url = input("enter Nitter RSS feeds url: ")

  with open(file_path, "w") as text_file:
    print("feeds_db: {}".format(feeds_db), file=text_file)
    print("feeds_db_user: {}".format(feeds_db_user), file=text_file)
    print("feeds_url: {}".format(feeds_url), file=text_file)

def create_table(db, db_user, table, sql):

  conn = None
  try:

    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()

    print("Creating table.. "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

#############################################################################################

# Load configuration from config file
config_filepath = "config/db_config.txt"
feeds_db = get_parameter("feeds_db", config_filepath)
feeds_db_user = get_parameter("feeds_db_user", config_filepath)
feeds_url = get_parameter("feeds_url", config_filepath)

############################################################
# create database
############################################################

conn = None

try:

  conn = psycopg2.connect(dbname='postgres',
      user=feeds_db_user, host='',
      password='')

  conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

  cur = conn.cursor()

  print("Creating database " + feeds_db + ". Please wait...")

  cur.execute(sql.SQL("CREATE DATABASE {}").format(
          sql.Identifier(feeds_db))
      )
  print("Database " + feeds_db + " created!")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

#############################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)
  # Load configuration from config file
  os.remove("db_config.txt")
  print("Exiting. Run db-setup again with right parameters")
  sys.exit(0)

if conn is not None:

  print("\n")
  print("Nitter feed parameters saved to db-config.txt!")
  print("\n")

############################################################
# Create needed tables
############################################################

print("Creating table...")

########################################

db = feeds_db
db_user = feeds_db_user
table = "tweets"
sql = "create table "+table+" (id bigint PRIMARY KEY, published timestamp, tootid bigint)"
create_table(db, db_user, table, sql)

#####################################

print("Done!")
print("Now you can run setup.py!")
print("\n")
