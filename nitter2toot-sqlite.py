import os
import sys
import feedparser
from mastodon import Mastodon
import sqlite3
from datetime import datetime, timedelta
import time

def date_diff_in_seconds(dt1, dt2):
    dt1 = datetime.strptime(dt1, '%Y-%m-%d %H:%M:%S')
    timedelta = dt2 - dt1
    days = timedelta.days
    return days, timedelta.days * 24 * 3600 + timedelta.seconds

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

########################################################
# main

if __name__ == '__main__':

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    feeds_db = get_parameter("feeds_db", config_filepath)
    feeds_url = get_parameter("feeds_url", config_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    now = datetime.now()
    publish = 0
    prev_feed_date = None
    days = 0
    seconds = None
    user = feeds_url.rstrip('/rss')
    user_index = user.rfind('/')
    user = '@' + user[user_index+1:]

    newsfeed = feedparser.parse(feeds_url)

    new_feeds_id = []
    new_feeds_title = []
    new_feeds_author = []
    new_feeds_link = []
    new_feeds_published = []

    for entry in reversed(newsfeed.entries[1:]):

        title = entry['title']
        author = entry['author_detail']['name']
        id = int(entry['id'].split('status/')[1][:-2])
        link = entry['link']
        published = entry['published']

        published_str = published.rstrip(' GMT')
        published_dt_ix = published_str.index(', ')
        published_dt = published_str[published_dt_ix + 2:]
        published_dt = datetime.strptime(published_dt, '%d %b %Y %H:%M:%S')

        if published_dt < now - timedelta(hours=12):

            continue

        try:

            conn = sqlite3.connect(feeds_db +'.db')

            cur = conn.cursor()

            get_id = (id,)
            cur.execute('select id from tweets where id=?', get_id)

            row = cur.fetchone()
            if row == None:
                new_feeds_id.append(id)
                new_feeds_title.append(title)
                new_feeds_author.append(author)
                new_feeds_link.append(link)
                new_feeds_published.append(published)

        except sqlite3.DatabaseError as db_error:

            print(db_error)
            sys.exit("Run 'python sqlite-setup.py'")

        finally:

            if conn is not None:

                conn.close()

    i = 0
    while i < len(new_feeds_id):

        feed_published = datetime.strptime(new_feeds_published[i], '%a, %d %b %Y %H:%M:%S GMT')

        title = new_feeds_title[i]
        author = new_feeds_author[i]
        id = new_feeds_id[i]
        link = new_feeds_link[i]

        ###################################################################
        # get last published toot id and published datetime
        ###################################################################

        try:

            conn = sqlite3.connect(feeds_db +'.db')

            cur = conn.cursor()

            cur.execute('select published, tootid from tweets order by id desc limit 1') #get previous toot id and publish date
            row = cur.fetchone()
            if row != None:
                prev_feed_date = row[0]
                prev_id = row[1]
            else:
                prev_feed_date = None
                prev_id = None
                seconds = 10

            cur.close()

        except:

            print("error")

        finally:

            if conn is not None:

                conn.close()

        ###########################################################

        if prev_feed_date != None:
            days, seconds = date_diff_in_seconds(prev_feed_date, feed_published)

        ###########################################################

        if "RT by " in title:
            toot_text = 'ha impulsat:\n'
            title = title.replace("RT by "+user+":"+" ","")
            toot_text += title+'\n'
        else:
            toot_text = str(title)+'\n'
        toot_text += str(link)+'\n'
        toot_text += '\n'
        toot_text += str(feed_published)

        print("Tooting...")
        print(toot_text)

        if len(new_feeds_id) > 1:

            if seconds is not None and days == 0 and seconds < 5:
                toot_id = mastodon.status_post(toot_text, in_reply_to_id=prev_id,)
            else:
                toot_id = mastodon.status_post(toot_text, in_reply_to_id=None,)
            time.sleep(2)

        else:

            if seconds is not None and days == 0 and seconds < 5:
                toot_id = mastodon.status_post(toot_text, in_reply_to_id=prev_id,)
                time.sleep(2)
            else:
                toot_id = mastodon.status_post(toot_text, in_reply_to_id=None,)

        #########################################################

        insert_sql = 'INSERT INTO tweets(id, published, tootid) VALUES (?,?,?) ON CONFLICT DO NOTHING'

        conn = sqlite3.connect(feeds_db +'.db')

        try:

            cur = conn.cursor()

            insert_values = (id, feed_published, toot_id['id'],)
            cur.execute(insert_sql, insert_values)

            update_values = (feed_published, toot_id['id'], id,)
            cur.execute('update tweets set published=?, tootid=? where id=?', update_values)

            conn.commit()

            cur.close()

        except:

            print("error")

        finally:

            if conn is not None:

                conn.close()

        i += 1

    print("No new feeds")
