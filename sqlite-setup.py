#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError
import sqlite3

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  if not os.path.exists('config'):
    os.makedirs('config')
  print("Setting up Nitter newsfeed parameters...")
  print("\n")
  feeds_db = input("feeds db name: ")
  feeds_url = input("enter Nitter RSS feeds url: ")

  with open(file_path, "w") as text_file:
    print("feeds_db: {}".format(feeds_db), file=text_file)
    print("feeds_url: {}".format(feeds_url), file=text_file)

def create_table(db, table, sql):

  conn = sqlite3.connect(db+'.db')
  try:

    cur = conn.cursor()

    print("Creating table.. "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except:

    print("error")

  finally:

    if conn is not None:

      conn.close()

#############################################################################################

# Load configuration from config file
config_filepath = "config/db_config.txt"
feeds_db = get_parameter("feeds_db", config_filepath)
feeds_url = get_parameter("feeds_url", config_filepath)

########################################

db = feeds_db
table = "tweets"
sql = "create table "+table+" (id bigint PRIMARY KEY, published timestamp, tootid bigint)"
create_table(db, table, sql)

#####################################

print("Done!")
print("Now you can run setup.py!")
print("\n")
